/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { singleFlingPager } from "@ohos/recyclerviewpager"
import {Toolbar} from './toolbar'
import { display } from '@kit.ArkUI'

@Entry
@ComponentV2
@Preview
struct singleFlingPagerActivity{

  @Local index: number= 0
  @Local offsetX: number= 0
  private arr: number[]= [1, 2, 3, 4, 5, 6, 7, 8, 9]
  private marginLeft: number= 50
  private marginRight: number= 0
  private Containderwidth: number= px2vp(display.getDefaultDisplaySync().width)
  private ContainderHeight: number|string = '100%'
  private ScreenOffset: number= 2800
  private fontSize: number= 30*2
  private offsetXItem: number = 25
  @Builder specificParam(item : ESObject) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + item.i).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft, right: this.marginRight })
      .width(this.Containderwidth - 2 * (this.marginLeft + this.marginRight) - 2 * this.offsetXItem)
      .height('80%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 + this.offsetX / this.ScreenOffset : 1 - this.offsetX / this.ScreenOffset
      })
      .offset({x:this.offsetXItem})
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft, right: this.marginRight })
      .width(this.Containderwidth - 2 * (this.marginLeft + this.marginRight) - 2 * this.offsetXItem)
      .height('50%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 - this.offsetX / this.ScreenOffset : 1 + this.offsetX / this.ScreenOffset
      })
      .offset({x:this.offsetXItem})
    }
  }

  build() {
    Column() {
      Toolbar({ title: 'Single Fling Pager', isBack: true })
      Flex({ direction: FlexDirection.Column }) {
        singleFlingPager(
          {
            arr: this.arr,
            offsetX: this.offsetX!!,
            index: this.index!!,
            marginLeft: this.marginLeft,
            marginRight: this.marginRight,
            Containderwidth: this.Containderwidth - 2 * (this.marginLeft + this.marginRight) - 2 * this.offsetXItem,
            ContainderHeight: this.ContainderHeight,
            content: (item : ESObject) => {
              this.specificParam(item)
            }
          }
        )
      }
    }
  }

}